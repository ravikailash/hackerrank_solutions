'''
Counter Game
https://www.hackerrank.com/challenges/counter-game/problem
'''

def counter_game(n):
   flip = 0
   while True:
      if n <= 1:
         break

      if n & (n-1) == 0:
         n //= 2
      else:
         n -= pow(2, n.bit_length()-1)

      flip ^= 1

   if flip == 0:
      return 'Richard'
   else:
      return 'Louise'
      

for _ in range(int(input())):
   n = int(input())
   print(counter_game(n))
