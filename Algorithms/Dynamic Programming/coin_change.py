'''
https://www.hackerrank.com/challenges/coin-change/problem
Can you determine the number of ways of making change for n units using the given types of coins?
Example:
   I/P: m = 4, C = [8, 3, 1, 2], n = 3
   O/P: {1, 1, 1}, {1, 2}, {3}
'''

import time

def change(coins, money, index, memo):
   if money == 0:
      return 1
   if index >= len(coins):
      return 0

   key = str(money) + '_' + str(index)
   if key in memo:
      return memo[key]

   amount_with_coins, ways = 0, 0

   while amount_with_coins <= money:
      remaining = money - amount_with_coins
      ways += change(coins, remaining, index + 1, memo)
      amount_with_coins += coins[index]

   memo[key] = ways
   return ways


def get_change(coins, money):
   return change(coins, money, 0, {})

def test():
   n, m = 20, 6
   coins = [50, 25, 10, 5, 2, 1]
   start_time = time.time()

   print(get_change(coins, n))
   end_time = time.time()
   print('Time taken: ', end_time - start_time)

   n, m = 3, 4
   coins = [8, 3, 1, 2]
   print(get_change(coins, n))

   n, m = 4, 3
   coins = [1, 3, 2]
   print(get_change(coins, n))

test()