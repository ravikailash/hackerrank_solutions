'''
The Maximum subarray
https://www.hackerrank.com/challenges/maxsubarray/problem
'''

import sys

def max_subarray_sum(array):
    # sys.stdout.writelines('%s\n'%' '.join(map(str, array)))

    # Contiguous subarray using kadane
    max_current = max_sofar = array[0]

    # Non contiguous subarray
    maxi = array[0]

    for item in array[1:]:
        max_current = max(item, max_current + item)
        max_sofar = max(max_sofar, max_current)

        maxi = max(maxi, maxi + item, item)

    sys.stdout.write('%d %d\n'%(max_sofar, maxi))

# ####################################################################
def test():
    arrays = [[1, -3, 2, 1, -1],
              [1, -2, 2, 3, 2],
              [-3, -4, 1, 9, 10, -1, 20, -20, 8],
              [-8, -7, -5, -2, -1],
              [-8, -7, -5, -2, -1, 0],
              [5, 4, -3, 8, 10],
              [1, 2, 3, 4],
              [2, -1, 2, 3, 4, -5]]

    for arr in arrays:
        max_subarray_sum(arr)


def main():
    T = int(sys.stdin.readline().strip())

    for _ in range(T):
        N = int(sys.stdin.readline().strip())
        array = [int(x) for x in sys.stdin.readline().strip().split(' ')]
        max_subarray_sum(array[:N])

# test()
main()