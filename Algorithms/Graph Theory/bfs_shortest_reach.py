#!/bin/python3

'''
Breadth First Search:Shortest Reach
https://www.hackerrank.com/challenges/bfsshortreach/problem
'''

import sys
edge_length = 6

def shortest_path(edges, n, start):
    path = [-1 for _ in range(n)]
    queue = [[start, 0]]
    visited = set()

    while not queue == []:
       curr_node, depth = queue.pop(0)
       if curr_node not in visited:
           visited.add(curr_node)
           path[curr_node] = depth
           for vertex in edges[curr_node]:
              queue.append([vertex, depth + edge_length])
    return path
           
    
if __name__ == "__main__":
    q = int(input().strip())
    for a0 in range(q):
       n, m = [int(x) for x in input().strip().split(' ')]
       edges = [set() for _ in range(n)]
       for a1 in range(m):
           u, v = [int(x) for x in input().strip().split(' ')]
           edges[u-1].add(v-1)
           edges[v-1].add(u-1)
       s = int(input().strip())
       
       path = shortest_path(edges, n, s-1)
       print(' '.join([str(x) for index, x in enumerate(path) if index+1 != s]))
