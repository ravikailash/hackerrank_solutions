'''
Encryption
https://www.hackerrank.com/challenges/encryption/problem

import math

def encrypt(string):
   s = ''.join([x for x in string.split(' ')])
   C = math.ceil(math.sqrt(len(s)))

   result = []
   for i in range(C):
      j = i; t = s[i]
      while j+C < len(s):
         t += s[j+C]
         j += C
      result.append(t)

   return ' '.join(result)

def simple_encrypt(s):
   s = ''.join(s.split(' '))
   c = math.ceil(math.sqrt(len(s)))
   return ' '.join(map(lambda x: s[x::c], range(c)))

test_cases = ['if man was meant to stay on the ground god would have given us roots', 
           'haveaniceday', 
           'feedthedog', 
           'chillout',
           'Three']

test_ops = ['imtgdvs fearwer mayoogo anouuio ntnnlvt wttddes aohghn sseoau', 
         'hae and via ecy',
         'fto ehg ee dd', 
         'clu hlt io', 
         'Te he r']

for i, case in enumerate(test_cases):
   res = encrypt(case.strip())
   print(res, ':', res == simple_encrypt(case))

