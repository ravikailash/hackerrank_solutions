'''
Connected Cells in a Grid
https://www.hackerrank.com/challenges/connected-cell-in-a-grid/problem
'''

def get_size(matrix, row, col):
    if 0 <= row < len(matrix) and 0 <= col < len(matrix[0]):
        if matrix[row][col] == 0:
            return 0

        matrix[row][col] = 0
        curr_size = 1
        for x in range(row-1, row+2):
            for y in range(col-1, col+2):
                if x != row or y != col:
                    curr_size += get_size(matrix, x, y)
        return curr_size
    
    return 0

def get_connected_region(matrix):
   n, m = len(matrix), len(matrix[0])
   # visited = [[False for _ in range(m)] for i in range(n)]
   max_region = 0

   for row in range(n):
      for col in range(m):
         if matrix[row][col] == 1:
            max_region = max(max_region, get_size(matrix, row, col))
   
   return max_region


def test():
   matrix = [[1, 1, 0, 0],
           [0, 1, 1, 0],
           [0, 0, 1, 0],
           [1, 0, 0, 0]]

   matrix2 = [[0, 0, 1, 1],
            [0, 1, 0, 1],
            [0, 1, 1, 0],
            [0, 0, 0, 1]]

   matrix3 = [[0, 0, 0, 1, 1, 0, 0],
            [0, 1, 0, 0, 1, 1, 0],
            [1, 1, 0, 1, 0, 0, 1],
            [0, 0, 0, 0, 0, 1, 0],
            [1, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 0, 0, 0]]

   print(get_connected_region(matrix))
   print(get_connected_region(matrix2))
   print(get_connected_region(matrix3))

def main():
   n = int(input())
   m = int(input())
   matrix = []

   for _ in range(n):
      row = input()
      matrix.append([int(x) for x in row.strip().split(' ')])

   print(get_connected_region(matrix))


# test()
main()