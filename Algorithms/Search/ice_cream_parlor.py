'''
Ice cream Parlor
https://www.hackerrank.com/challenges/icecream-parlor/problem
'''

def find_choices(menu, money):
  res = []
  menu_cache = {}

  for index, item in enumerate(menu):
    if money - item in menu_cache:
      res.extend([menu_cache[money - item], index])
      break
    else:
      menu_cache[item] = index
  
  return ' '.join([str(x+1) for x in res])

def test():
  test_cases = [{'m': 4, 'costs': '1 4 5 3 2'}, 
          {'m': 4, 'costs': '2 2 4 3'},
          {'m': 4, 'costs': '2 3 4 3'}]

  test_money  =[0, 3, 4, 6, 7, 10]

  for case in test_cases:
    menu = [int(x) for x in case['costs'].strip().split(' ')]
    # money = case['m']
    print(menu)
    for money in test_money:
      print(money, ':', find_choices(menu, money))

def main():
  cases = int(input())
  for _ in range(cases):
      money = int(input())
      n = int(input())
      menu = [int(x) for x in input().strip().split(' ')]
      print(find_choices(menu, money))
      

# test()
main()