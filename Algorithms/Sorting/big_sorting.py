'''
Big Sorting
https://www.hackerrank.com/challenges/big-sorting/problem
'''

def big_sorting(array):
   if len(array) == 1:
      return array

   mid = len(array) // 2
   left = big_sorting(array[:mid])
   right = big_sorting(array[mid:])
   return merge(left, right)


def merge(left, right):
   if left == [] or right == []:
      return left or right
   
   i, j = 0, 0
   res = []
   
   while i < len(left) and j < len(right):
      if int(left[i]) < int(right[j]):
         res.append(left[i])
         i += 1
      else:
         res.append(right[j])
         j += 1

   if i < len(left):
      res.extend(left[i:])

   if j < len(right):
      res.extend(right[j:])

   return res


array = ['31415926535897932384626433832795', '1', '3', '10', '3', '5']

b = array.copy()

# solution-1 without changing the original array
print(sorted(array, key= lambda item : int(item)))

# solution-2 by changing the original array
array.sort(key=lambda item: int(item))
print(array)

# solution-3 by using merge sorting 
print(big_sorting(b))
