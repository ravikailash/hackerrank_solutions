'''
Making Anagrams
https://www.hackerrank.com/challenges/making-anagrams/problem
'''

def anagrams(a, b):
   if a == '' or b == '':
      return max(len(a), len(b))
   if a == b:
      return 0

   a_dict, b_dict = {}, {}
   i, j = 0, 0
   while i < len(a) or j < len(b):
      if i < len(a):
         a_dict[a[i]] = a_dict.get(a[i], 0) + 1
         i += 1

      if j < len(b):
         b_dict[b[j]] = b_dict.get(b[j], 0) + 1
         j += 1

   diff = 0
   for key, value in a_dict.items():
      if b_dict.get(key, False):
         diff += abs(value - b_dict[key])
         b_dict.pop(key)
      else:
         diff += value

   for _, value in b_dict.items():
      diff += value

   return diff

print(anagrams('abc', 'cde'))
print(anagrams('abc', 'cdde'))
print(anagrams('bacdc', 'dcbac'))
print(anagrams('bacdc', 'dcbad'))
