'''
Tries: Contacts
https://www.hackerrank.com/challenges/ctci-contacts/problem
'''

class Node:
   def __init__(self, data=None, children=None, is_word=False):
      self.data = data
      self.children = {}
      self.is_word = is_word
      self.prefix = 0
   
   def get_children(self):
      return [self.children[child] for child in self.children]

class Trie:
   def __init__(self):
      self.root = Node('/')
   
   def add(self, contact):
      current_node = self.root
      for index in range(len(contact)):
         if contact[index] in current_node.children:
            current_node = current_node.children[contact[index]]
            current_node.prefix += 1
         else:
            break

      while index < len(contact):
         new_node = Node(contact[index])
         current_node.children[contact[index]] = new_node
         index +=1
         current_node = new_node
         current_node.prefix += 1
         
      current_node.is_word = True
   
   def find(self, prefix):
      current_node = self.root
      for index in range(len(prefix)):
         if prefix[index] in current_node.children:
            current_node = current_node.children[prefix[index]]
         else:
            return 0

      return current_node.prefixs

contacts = Trie()
contacts.add('hackerrank')
contacts.add('hack')
print(contacts.find('hac'))
print(contacts.find('hack'))
print(contacts.find('hacr'))
print(contacts.find('rank'))
print(contacts.find('hak'))
