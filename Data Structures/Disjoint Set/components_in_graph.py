'''
Print the smallest and largest connected components in a graph
https://www.hackerrank.com/challenges/components-in-graph/problem
'''

import sys


class Subset:
    def __init__(self, node, parent=None):
        self.node = node
        self.parent = parent if parent is not None else node
        self.rank = 1
        self.size = 1

    def __str__(self):
        return '{}-{}-{}:{}'.format(self.node, self.parent, self.rank, self.size)


def find_parent(subsets, node):
    if subsets[node].parent == node:
        return node
    return find_parent(subsets, subsets[node].parent)


def union(subsets, x, y):
    subsets[y].parent = x
    subsets[x].rank += 1
    subsets[x].size += subsets[y].size


def add_nodes(subsets, x, y):
    x_root = find_parent(subsets, x)
    y_root = find_parent(subsets, y)

    if x_root == y_root:
        return

    if subsets[x_root].rank >= subsets[y_root].rank:
        union(subsets, x_root, y_root)
    elif subsets[x_root].rank < subsets[y_root].rank:
        union(subsets, y_root, x_root)


def find_connected_components(subsets):
    sizes = [sys.maxsize, -sys.maxsize]
    # print(subsets)
    for _, item in subsets.items():
        item_size = subsets[find_parent(subsets, item.node)].size
        if item_size < sizes[0] and item_size != 1:
            sizes[0] = item_size
        if item_size > sizes[1]:
            sizes[1] = item_size
    return sizes

def main():
    N = 5
    components = ['1 6', '2 7', '3 8', '4 9', '2 9']
    subsets = {}
    for _, comp in enumerate(components):
        edge = [int(x) for x in comp.strip().split(' ')]
        if edge[0] not in subsets:
            subsets[edge[0]] = Subset(edge[0])
        if edge[1] not in subsets:
            subsets[edge[1]] = Subset(edge[1])
        add_nodes(subsets, edge[0], edge[1])

    print(find_connected_components(subsets))

def test():
    f = open('components_in_graph_input.txt', 'r')
    N = int(f.readline().strip())

    subsets = {}
    for line in f:
        u, v = [int(x) for x in line.strip().split(' ')]
        if u not in subsets:
            subsets[u] = Subset(u)
        if v not in subsets:
            subsets[v] = Subset(v)
        add_nodes(subsets, u, v)

    print(find_connected_components(subsets))
    f.close()

if __name__ == '__main__':
    main()
    test()