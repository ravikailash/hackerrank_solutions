'''
Merging communities
https://www.hackerrank.com/challenges/merging-communities/problem
'''

import time
import sys

class Subset:
    def __init__(self, data, parent=None):
        self.data = data
        self.parent = parent if parent is not None else data
        self.rank = 1
        self.size = 1

def find_parent(subsets, node):
    if subsets[node].parent == node:
        return node
    return find_parent(subsets, subsets[node].parent)

def union(subsets, x, y):
    subsets[y].parent = x
    subsets[x].size += subsets[y].size
    subsets[x].rank += 1

def add_community(subsets, x, y):
    x_root = find_parent(subsets, x)
    y_root = find_parent(subsets, y)

    if x_root == y_root:
        return

    if subsets[x_root].rank >= subsets[y_root].rank:
        union(subsets, x_root, y_root)
    elif subsets[x_root].rank < subsets[y_root].rank:
        union(subsets, y_root, x_root)

def find_community_size(subsets, node):
    node_parent = find_parent(subsets, node)
    return subsets[node_parent].size

def main():
    community = [3, 5]
    queries = [['Q 1', 'M 1 2', 'Q 2', 'M 2 3', 'Q 3', 'Q 2'],
               ['Q 1', 'M 1 2', 'Q 2', 'M 2 3', 'Q 3', 'Q 2', 'Q 5', 'M 4 5', 'Q 5', 'Q 2', 'M 3 4', 'Q 2', 'Q 5']]

    for index in range(len(community)):
        print('Community :', community[index])
        subsets = [Subset(x) for x in range(community[index])]
        query = queries[index]
        for q in query:
            q = q.strip().split(' ')
            if q[0] == 'M':
                add_community(subsets, int(q[1])-1, int(q[2])-1)
            elif q[0] == 'Q':
                find_community_size(subsets, int(q[1])-1)


def test():
    f =  open('merging_community_input.txt', 'r')
    results = open('merging_community_output.txt', 'r')

    community, N = f.readline().strip().split(' ')

    start_time = time.time()
    subsets = [Subset(node) for node in range(int(community))]
    create_set_time = time.time()
    mismatch = False

    print(community, N)
    for index, line in enumerate(f):
        query = line.strip().split(' ')
        if query[0] == 'M':
            add_community(subsets, int(query[1])-1, int(query[2])-1)
        elif query[0] =='Q':
            comm_size = find_community_size(subsets, int(query[1])-1)
            if comm_size != int(results.readline().strip()):
                mismatch = True
                break

    end_time = time.time()
    if not mismatch:
        print('All ok')
    else:
        print('error at:', index)

    print('Set-creation:', create_set_time - start_time)
    print('Resule-time :', end_time - create_set_time)
    f.close()
    results.close()


if __name__ == '__main__':
    # main()
    test()