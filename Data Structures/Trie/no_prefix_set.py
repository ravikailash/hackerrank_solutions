'''
No Prefix Set
https://www.hackerrank.com/challenges/no-prefix-set/problem
'''

class Node:
    def __init__(self, data, is_word=False):
        self.data = data
        self.children = {}
        self.is_word = is_word
        
class Trie:
    def __init__(self):
        self.root = Node('/')
    
    def check(self, word):
        current = self.root
        index = 0
        while index < len(word):
            if word[index] in current.children:
                current = current.children[word[index]]
                index += 1
            else:
                break
                
        if (current.is_word is True and current != self.root):
            return True
        
        if index == len(word):
            return True
        
        while index < len(word):
            new_node = Node(word[index])
            current.children[word[index]] = new_node
            index += 1
            current = new_node
        
        current.is_word = True
        return False

prefix = Trie()
n = int(input())
flag, flag_string = False, None

for _ in range(n):
    item = input().strip()
    found = prefix.check(item)
    if found is True and flag is False:
        flag = True
        flag_string = item
        break

if flag is True:
    print('BAD SET')
    print(flag_string)
else:
    print('GOOD SET')