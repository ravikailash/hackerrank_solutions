'''
Tries: Contacts
https://www.hackerrank.com/challenges/contacts/problem
'''

class Node:
    def __init__(self, data=None, children=None, is_word=False):
        self.data = data
        self.children = [None for _ in range(26)]
        self.is_word = ('False', '')

    def get_children(self):
        return [x for x in self.children if x is not None]

class Trie:
    def __init__(self):
        self.root = Node('/')

    def add(self, contact):
        current = self.root
        for index in range(len(contact)):
            if current.children[ord(contact[index]) - ord('a')] is not None:
                current = current.children[ord(contact[index]) - ord('a')]
            else:
                break

        while index < len(contact):
            new_node = Node(contact[index])
            current.children[ord(contact[index]) - ord('a')] = new_node
            current = new_node
            index += 1
        
        current.is_word = (True, contact)

    def find(self, prefix):
        current = self.root

        for index in range(len(prefix)):
            if current.children[ord(prefix[index]) - ord('a')] is not None:
                current = current.children[ord(prefix[index]) - ord('a')]
            else:
                return 0

        count = 0
        queue = [current]

        while queue:
            cur_node = queue.pop()
            if cur_node.is_word[0] is True:
                count += 1
            queue.extend(cur_node.get_children())

        return count


n = int(input().strip())
contacts = Trie()
for a0 in range(n):
    op, name = input().strip().split(' ')
    if op == 'add':
        contacts.add(name)
    if op == 'find':
        print(contacts.find(name))
        