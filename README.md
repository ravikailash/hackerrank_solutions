# HackerRank Solutions
Solutions to the Hackerrank problems


### Contents
* Algorithms  
	* `Bit Manipulation`
		* Counter game => [Problem](https://www.hackerrank.com/challenges/counter-game/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Bit%20Manipulation/counter_game.py)

	* `Dynamic Programming`
		* The Coin Change Problem => [Problem](https://www.hackerrank.com/challenges/coin-change/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Dynamic%20Programming/coin_change.py)
		* The Maximum Subarray => [Problem](https://www.hackerrank.com/challenges/maxsubarray/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Dynamic%20Programming/maximum_subarray.py)
	
	* `Graph Theory`
		* Breadth First Search: Shortest Reach => [Problem](https://www.hackerrank.com/challenges/bfsshortreach/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Graph%20Theory/bfs_shortest_reach.py)

	* `Implementation`
		* Encryption => [Problem](https://www.hackerrank.com/challenges/encryption/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Implementation/encryption.py)
	
	* `Search`
		* Connected Cells in a Grid => [Problem](https://www.hackerrank.com/challenges/connected-cell-in-a-grid/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Search/connected_cells_in_grid.py)
		* Ice Cream parlor => [Problem](https://www.hackerrank.com/challenges/icecream-parlor/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Search/ice_cream_parlor.py)

	* `Sorting`
		* Big Sorting => [Problem](https://www.hackerrank.com/challenges/big-sorting/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Sorting/big_sorting.py)

	* `Strings`
		* Making Anagrams => [Problem](https://www.hackerrank.com/challenges/making-anagrams/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Algorithms/Strings/making_anagrams.py)

* CTCI  
	* Tries: Contacts =>  [Problem](https://www.hackerrank.com/challenges/ctci-contacts/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/CTCI/tries_contacts_dict.py)

* Data Structures
	* `Disjoint Set`  
		* Components in a graph => [Problem](https://www.hackerrank.com/challenges/components-in-graph/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Data%20Structures/Disjoint%20Set/components_in_graph.py)
		* Merging Communities => [Problem](https://www.hackerrank.com/challenges/merging-communities/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Data%20Structures/Disjoint%20Set/merging_communities.py)

	* `Trie`
		* No Prefix Set => [Problem](https://www.hackerrank.com/challenges/no-prefix-set/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Data%20Structures/Trie/no_prefix_set.py)
		* Contacts => [Problem](https://www.hackerrank.com/challenges/contacts/problem) | [Solution](https://gitlab.com/ravikailash/hackerrank_solutions/blob/master/Data%20Structures/Trie/tries_contacts_array.py)